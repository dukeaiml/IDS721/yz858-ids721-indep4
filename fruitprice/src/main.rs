
use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};
use std::collections::HashMap;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};

/// Define a fruit:price map for lambda function to use
lazy_static! {
    static ref FRUITS_INFO: HashMap<String, f64> = {
        let mut map = HashMap::new();
        map.insert("apple".to_string(), 3.0);
        map.insert("orange".to_string(), 2.0);
        map.insert("pear".to_string(), 1.5);
        map
    };
}

#[derive(Deserialize)]
struct Request {
    fruitlist: Vec<String>,
}

#[derive(Serialize)]
struct Response {
    pricelist: Vec<f64>,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let fruitlist = event.payload.fruitlist;
    let mut fruitpricelist : Vec<f64> = Vec::new();

    for fruit in fruitlist.iter() {
        if let Some(&value) = FRUITS_INFO.get(fruit) {
            println!("Value of {} is: {}", fruit, value);
            fruitpricelist.push(value);
        }
    }

    let resp = Response {
        pricelist: fruitpricelist
    };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
