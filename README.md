# yz858-ids721-indep4

In mini-project 4, I created two AWS Lambda functions using Rust to calculate the total price of products.

The first lambda function in the **/fruitprice** folder accesses the price list and returns a list that contains the price for each fruit given.

The second lambda function takes in this list and calculate the total price of all the fruits.

This project is adapted from mini-project2 where functionalties are integrated into one lambda function. To apply step function workflow, the functionalities are now broken into 2, with each one working on a specific part of the original integrated functionlity.

## Requirements
- [x] Lambda Functionality (30pt): Rust Lambda Function using Cargo Lambda
- [x] Step function workflow coordinating Lambdas (30pt)
- [x] Orchestrating data processing pipeline (20pt)
- [x] Documentation (10pt)
- [x] Demo video (10pt)

## Steps 
(Reference: https://www.cargo-lambda.info/guide/getting-started.html)

1. Install **Cargo Lambda**
```
brew tap cargo-lambda/cargo-lambda
brew install cargo-lambda
```

2. Create new project
```
cargo lambda new mini2 \
    && cd mini2
```

3. Add dependencies
```
cargo add DEPENDENCY
```

4. Test
```
// Test fruitprice
cargo lambda watch
cargo lambda invoke --data-ascii "{ \"fruitlist\": [\"apple\",\"orange\"] }" 

//Test addprice
cargo lambda watch
cargo lambda invoke --data-ascii "{ \"pricelist\": [5.0,3.1] }"
```

5. Build and Deploy
```
cargo lambda build --release
cargo lambda deploy
```

## Screenshots
1. Lambda functions
![Lambdas](screenshots/Lambdas.png)

2. Successful execution of step function
![Deploy](screenshots/Execution.png)

2. Step function diagram
![Deploy](screenshots/StateDiagram.png)

## Demo Video
https://duke.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=47bd5981-f8dc-44f0-8624-b15a0123f98b

