
use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};

use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct Request {
    pricelist: Vec<f64>,
}

#[derive(Serialize)]
struct Response {
    totalprice: f64
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let pricelist = event.payload.pricelist;
    let mut totalprice = 0.0;

    for price in pricelist.iter() {
        totalprice += price;
    }

    let resp = Response {
        totalprice: totalprice
    };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
